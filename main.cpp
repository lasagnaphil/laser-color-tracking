#include <iostream>
#include <fstream>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;

int main( int argc, char** argv )
{
    ofstream dataStream;
    string dataName = string(argv[1]);
    dataName.erase(0,7);
    dataName = "data_" + dataName + string(".txt");
    dataStream.open(dataName);

    VideoCapture cap(argv[1]);
    //VideoCapture cap("videos/20151203_152749.mp4"); //capture the video from web cam

    if ( !cap.isOpened() )  // if not success, exit program
    {
        cout << "Cannot open the video file" << endl;
        return -1;
    }

    float vSizeX = 3840.0f;
    float vSizeY = 2160.0f;
    //Rect cropRect = Rect((int)(vSizeX*0.0f),(int)(vSizeY*0.0f),(int)(vSizeX*1.0f),(int)(vSizeY*1.0f));
    Rect cropRect = Rect(1107,0,2545,2160);

    namedWindow("Control", WINDOW_NORMAL); //create a window called "Control"
    resizeWindow("Control", 800, 600); //resize window

    int iLowH = 0;
    int iHighH = 161;

    int iLowS = 0;
    int iHighS = 54;

    int iLowV = 100;
    int iHighV = 255;

    //Create trackbars in "Control" window

    cvCreateTrackbar("WindowPosX", "Control", &cropRect.x, (int)(vSizeX*1.0f));
    cvCreateTrackbar("WindowPosY", "Control", &cropRect.y, (int)(vSizeX*1.0f));
    cvCreateTrackbar("WindowSizeX", "Control", &cropRect.width, (int)(vSizeX*1.0f));
    cvCreateTrackbar("WindowSizeY", "Control", &cropRect.height, (int)(vSizeX*1.0f));

    cvCreateTrackbar("LowH", "Control", &iLowH, 179); //Hue (0 - 179)
    cvCreateTrackbar("HighH", "Control", &iHighH, 179);

    cvCreateTrackbar("LowS", "Control", &iLowS, 255); //Saturation (0 - 255)
    cvCreateTrackbar("HighS", "Control", &iHighS, 255);

    cvCreateTrackbar("LowV", "Control", &iLowV, 255); //Value (0 - 255)
    cvCreateTrackbar("HighV", "Control", &iHighV, 255);

    int iLastX = -1;
    int iLastY = -1;

    Mat imgTmp;
    cap.read(imgTmp);
    Mat croppedImgTmp = imgTmp(cropRect);

    Mat imgLines = Mat::zeros(croppedImgTmp.size(), CV_8UC3);
    Mat allImgLines = Mat::zeros(croppedImgTmp.size(), CV_8UC3);

    while (true)
    {
        Mat imgOriginal;

        bool bSuccess = cap.read(imgOriginal); // read a new frame from video

        if (!bSuccess) //if not success, break loop
        {
            cout << "End of video or cannot read a frame from video stream" << endl;
            break;
        }

        // crop the frame with the rect we desire
        Mat croppedImgOriginal = imgOriginal(cropRect);

        Mat imgHSV;

        cvtColor(croppedImgOriginal, imgHSV, COLOR_BGR2HSV); //Convert the captured frame from BGR to HSV

        Mat imgThresholded;

        inRange(imgHSV, Scalar(iLowH, iLowS, iLowV), Scalar(iHighH, iHighS, iHighV), imgThresholded); //Threshold the image

        //morphological opening (remove small objects from the foreground)
        erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );
        dilate( imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );

        //morphological closing (fill small holes in the foreground)
        dilate( imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );
        erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );

        Moments oMoments = moments(imgThresholded);

        double dM01 = oMoments.m01;
        double dM10 = oMoments.m10;
        double dArea = oMoments.m00;

        if (dArea > 10000)
        {
            int posX = dM10 / dArea;
            int posY = dM01 / dArea;

            if (iLastX >= 0 && iLastY >= 0 && posX >= 0 && posY >= 0)
            {
                line(imgLines, Point(posX, posY), Point(iLastX, iLastY), Scalar(0,0,255), 2);
            }

            iLastX = posX;
            iLastY = posY;

            dataStream << to_string(posX) << ", " << to_string(posY) << "\n";
        }

        namedWindow("Thresholded Image", WINDOW_NORMAL);
        resizeWindow("Thresholded Image", 1280, 720);
        imshow("Thresholded Image", imgThresholded); //show the thresholded image
        namedWindow("Original", WINDOW_NORMAL);
        resizeWindow("Original", 1280, 720);
        //croppedImgOriginal = croppedImgOriginal + imgLines;
        imshow("Original", croppedImgOriginal); //show the original image
        namedWindow("Path", WINDOW_NORMAL);
        resizeWindow("Path", 1280, 720);
        allImgLines = allImgLines + imgLines;
        imshow("Path", allImgLines);
        int key = waitKey(1);

        if (key == 27) //If 'esc' key is pressed, break loop
        {
            cout << "esc key is pressed by user" << endl;
            break;
        }
        if (key == 114)
        {
            cout << "resetting lines" << endl;
            allImgLines = Mat::zeros(croppedImgTmp.size(), CV_8UC3);
        }
    }

    return 0;

}
